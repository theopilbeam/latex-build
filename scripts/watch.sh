#!/usr/bin/env bash

trap "exit" INT TERM ERR
trap "echo 'Cleaning up...'; clean; kill 0" EXIT

shopt -s extglob

case "$(uname)" in
    (*Darwin*) rlCmd='greadlink'; ;;
    (*) rlCmd='readlink'; ;;
esac;

DIR="$(/usr/bin/dirname "$("$rlCmd" -f -- "$0")")"

declare -a existingTexFiles

for f in "$DIR"/../*.tex
do
    existingTexFiles+=($f)
done

declare -a generatedTexFiles

function clean {
    latexmk -C > /dev/null 2>&1

    for i in "${generatedTexFiles[@]}"
    do
	rm "$i"
    done
}

for f in "$DIR"/../*.md
do
    echo "Processing $f"
    texname="${f%.*}.tex"

    ls "$f" | entr pandoc -o "$texname" -f markdown -t latex "$f" &

    generatedTexFiles+=($texname)
done

for f in "${existingTexFiles[@]}"
do
  echo "Processing $f"

  latexmk -pdf -pvc "$f" &
done

wait
