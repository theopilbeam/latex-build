#!/usr/bin/env bash

shopt -s extglob

case "$(uname)" in
    (*Darwin*) rlCmd='greadlink'; ;;
    (*) rlCmd='readlink'; ;;
esac;

DIR="$(/usr/bin/dirname "$("$rlCmd" -f -- "$0")")"

declare -a existingTexFiles

for f in "$DIR"/../*.tex
do
    existingTexFiles+=($f)
done

declare -a generatedTexFiles

for f in "$DIR"/../*.md
do
    echo "Processing $f"
    texname="${f%.*}.tex"

    pandoc -o "$texname" -f markdown -t latex "$f"

    generatedTexFiles+=($texname)
done

for f in "${existingTexFiles[@]}"
do
  echo "Processing $f"

  latexmk -pdf "$f"
done

echo "Cleaning up..."
latexmk -c

for i in "${generatedTexFiles[@]}"
do
    rm "$i"
done
