Will continuously convert any `.md` files into `.tex` files using pandoc

- Tested on macOS only
- Uses [Skim](https://skim-app.sourceforge.io/) (`brew cask install skim`)
  - Or change `$pdf_previewer` in `.latexmkrc`
- Requires GNU Coreutils (`brew install coreutils`)
- Requires entr (`brew install entr`)

## Live reloading

- `./scripts/watch.sh`


## CI

When used with a gitlab repo, all .tex files will be complied & exported as artificats

## Offline build

- `./scripts/build.sh`
